import * as planck from 'planck';
import { toMeters, toPixels } from './planckUtils';
 
// this class extends planck Phaser Sprite class
export class PlanckBall extends Phaser.GameObjects.Sprite {

    // planck body
    planckBody : planck.Body;
    
    // ball speed
    speed : number;

    // last collided edge, to prevent colliding with the same edge twice
    lastCollidedEdge : planck.Edge;

    // angle to reflect after a collision
    angleToReflect : number;
 
    constructor(scene : Phaser.Scene, world : planck.World, posX : number, posY : number, radius : number, key : string) {
 
        super(scene, posX, posY, key);
         
        // adjust sprite display width and height
        this.displayWidth = radius * 2;
        this.displayHeight = radius * 2;
 
        // add sprite to scene
        scene.add.existing(this);
     
        // this is how we create a generic Box2D body
        this.planckBody  = world.createBody();

        // set the body as bullet for continuous collision detection
        this.planckBody.setBullet(true)
 
        // Box2D bodies are created as static bodies, but we can make them dynamic
        this.planckBody.setDynamic();
 
        // a body can have one or more fixtures. This is how we create a circle fixture inside a body
        this.planckBody.createFixture(planck.Circle(toMeters(radius)));
  
        // now we place the body in the world
        this.planckBody.setPosition(planck.Vec2(toMeters(posX), toMeters(posY)));

        // time to set mass information
        this.planckBody.setMassData({
 
            // body mass
            mass : 1,
 
            // body center
            center : planck.Vec2(),
  
            // I have to say I do not know the meaning of this "I", but if you set it to zero, bodies won't rotate
            I : 1
        });
    }

    // method to set a random velocity, given a speed
    setRandomVelocity(speed : number) : void {

        // save the speed as property
        this.speed = speed;
         
        // set a random angle
         let angle : number = Phaser.Math.Angle.Random();

         // set ball linear velocity according to angle and speed
         this.planckBody.setLinearVelocity(new planck.Vec2(this.speed * Math.cos(angle), this.speed * Math.sin(angle)))
    }

    // method to check if an edge is the one we already collided with
    sameEdgeCollision(edge : planck.Edge) : boolean {

        // check if current edge is the same as last collided edge
        let result : boolean = this.lastCollidedEdge == edge;

        // update last collided edge
        this.lastCollidedEdge = edge;

        // return the result
        return result;   
    }

    // method to handle bounce increasing speed number
    handleBounce(angle : number, speedIncrease : number) : void {

        // increase speed
        this.speed += speedIncrease;

        // set linear velocity according to angle and speed
        this.planckBody.setLinearVelocity(new planck.Vec2(this.speed * Math.cos(angle), this.speed * Math.sin(angle)));
    }

    // method to determine bounce angle against a line
    determineBounceAngle(line : Phaser.Geom.Line) : void {

        // get linear velocity
        let velocity : planck.Vec2 = this.planckBody.getLinearVelocity();

        // transform linear velocity into a line
        let ballLine : Phaser.Geom.Line = new Phaser.Geom.Line(0, 0, velocity.x, velocity.y);

        // calculate reflection angle between two lines
        this.angleToReflect = Phaser.Geom.Line.ReflectAngle(ballLine, line);   
    }

    // method to update ball position
    updatePosition() : void {
        
        // get ball planck body position
        let ballBodyPosition : planck.Vec2 = this.planckBody.getPosition();  

        // update ball position
        this.setPosition(toPixels(ballBodyPosition.x), toPixels(ballBodyPosition.y));  
    }
}