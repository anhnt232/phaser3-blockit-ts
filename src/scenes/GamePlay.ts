// THE GAME ITSELF

// enum to represent the game states
enum GameState {
    Waiting,
    Playing,
    Over    
}

import * as planck from 'planck';
import { GameOptions } from './gameOptions';
import { PlanckCompound } from './planckCompound';
import { PlanckBall } from './planckBall';
import { toPixels } from './planckUtils';

import CONFIG from "../../config";

// this class extends Scene class
export default class GamePlay extends Phaser.Scene {

    // Box2d world
    world : planck.World;

    // the ball
    theBall : PlanckBall;

    // planck compound object
    theCompound : PlanckCompound;

    // graphics objects to render the compound object
    compoundGraphics : Phaser.GameObjects.Graphics;

    // keep track of the number of bounces to restart the demo at 50
    score : number;
    bestScore : number;

    // variable to store current game state
    gameState : GameState;

    // variable to save the time the player touched the input to activate the wall
    downTime : number;

    // walls energy
    energy : number;

    scoreText: Phaser.GameObjects.BitmapText;
    bestScoreText: Phaser.GameObjects.BitmapText;
    logo: Phaser.GameObjects.Sprite;

    // constructor
    constructor() {
        super({
            key: 'GamePlay'
        });
    }

    // method to be executed when the scene has been created
    create() : void {

        // game state is set to "Waiting" (for player input)
        this.gameState = GameState.Waiting;

        // fill the energy according to game options
        this.energy = GameOptions.energy;

        // zero bounces at the beginning
        this.score = 0;
        
        this.bestScore = localStorage.getItem(CONFIG.localStorageName) == null ? 0 : Number(localStorage.getItem(CONFIG.localStorageName));

        let x = 150
        let y = 10
        var scorelabels = this.add.sprite(x, y, "scorelabels");
        scorelabels.setOrigin(0,0);
        scorelabels.setScale(0.8);
        var scorepanel = this.add.sprite(x, y+30, "scorepanel");
        scorepanel.setOrigin(0,0);
        scorepanel.setScale(0.8);
        this.scoreText = this.add.bitmapText( x+15, y+50, "font", "0", 50);
        this.scoreText.setOrigin(0, 0);
        this.bestScoreText = this.add.bitmapText( x+290, y+50, "font", this.bestScore.toString(), 50);
        this.bestScoreText.setOrigin(0, 0);
        
        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height -10, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(1);

        // just a couple of variables to store game width and height
        let gameWidth : number = this.scale.width as number;
        let gameHeight : number = this.scale.height as number;
    
        // world gravity, as a Vec2 object. It's just a x, y vector
        let gravity = new planck.Vec2(0, 0); 
         
        // this is how we create a Box2D world
        this.world = new planck.World(gravity);

        // add the planck ball
        this.theBall = new PlanckBall(this, this.world, gameWidth / 2, gameHeight / 2, GameOptions.ballRadius, 'ball');

        // add the planck compound object
        this.theCompound = new PlanckCompound(this, this.world, gameWidth / 2, gameHeight / 2, GameOptions.wallDistanceFromCenter, GameOptions.wallTickness);

        // add simulation graphics
        this.compoundGraphics = this.add.graphics();
        
        // add an event listener waiting for a contact to solve
        this.world.on('post-solve', () => {

            // handle ball bounce increasing speed
            this.theBall.handleBounce(this.theBall.angleToReflect, GameOptions.ballSpeedIncrease);
            
            // increase number of bounces
            this.updateScore();

        });

        // add an event listener waiting for a contact to pre solve
        this.world.on('pre-solve', (contact : planck.Contact) => {

            // if the compound is not active or if it's game over...
            if (!this.theCompound.active || this.gameState == GameState.Over) {

                // do not enable the contact
                contact.setEnabled(false);

                // if it's not game over...
                if (this.gameState != GameState.Over) {

                    // ...now it is! :)
                    this.gameState = GameState.Over

                    // create a particle manager with the same image used for the ball
                    let particleEmitter : Phaser.GameObjects.Particles.ParticleEmitterManager = this.add.particles('ball');

                    // create a particle emitter
                    let emitter = particleEmitter.createEmitter({
                        
                        // particle life span, in milliseconds
                        lifespan : 1500,

                        // particle speed range
                        speed : {
                            min : 10,
                            max : 30
                        },

                        // particle alpha tweening
                        alpha : {
                            start : 0.8,
                            end : 0.2
                        },

                        // particle scale tweening
                        scale : {
                            start : 0.1,
                            end : 0.05
                        }
                    });

                    // let the emitter fire 100 particles at ball position
                    emitter.explode(100, this.theBall.x, this.theBall.y);

                    // hide the ball
                    this.theBall.visible = false;

                    // add a time event
                    this.time.addEvent({

                        // start in 2 seconds
                        delay : 2000,

                        // callback function scope
                        callbackScope : this,

                        // callback function
                        callback: () => {

                            // restart the game
                            CONFIG.currentScore = this.score;
                            this.scene.start('GameOver');
                        }
                    });
                }

                // exit the method
                return;     
            }

            // get edge fixture in this circle Vs edge contact
            let edgeFixture : planck.Fixture = this.getEdgeFixture(contact);

            // get edge body
            let edgeBody : planck.Body = edgeFixture.getBody();

            // get edge shape
            let edgeShape : planck.Edge = edgeFixture.getShape() as planck.Edge;

            // did the ball just collided with this edge?
            if (this.theBall.sameEdgeCollision(edgeShape)) {
                
                // disable the contact
                contact.setEnabled(false);

                // exit callback function
                return;    
            }

            // get edge shape vertices
            let worldPoint1 : planck.Vec2 = edgeBody.getWorldPoint(edgeShape.m_vertex1);
            let worldPoint2 : planck.Vec2 = edgeBody.getWorldPoint(edgeShape.m_vertex2);
            
            // transform the planck edge into a Phaser line
            let worldLine : Phaser.Geom.Line = new Phaser.Geom.Line(toPixels(worldPoint1.x), toPixels(worldPoint1.y), toPixels(worldPoint2.x), toPixels(worldPoint2.y));
            
            // determine bounce angle
            this.theBall.determineBounceAngle(worldLine); 
        });

        // wait for input start to call activateWalls method
        this.input.on('pointerdown', this.activateWalls, this);

        // wait for input end to call deactivateWalls method
        this.input.on('pointerup', this.deactivateWalls, this); 
        
    }

    updateScore() : void {
        
        this.score ++;
        if (this.bestScore < this.score) {
            this.bestScore = this.score;
            localStorage.setItem(CONFIG.localStorageName, this.bestScore.toString());
            this.bestScoreText.setText(this.bestScore.toString());
        }
        this.scoreText.setText(this.score.toString());
        
    }

    // method to activate compound walls
    activateWalls(e : Phaser.Input.Pointer) : void {

        // is the game state set to "Waiting" (for player input)?
        if (this.gameState == GameState.Waiting) {

            // give the ball a random velocity
            this.theBall.setRandomVelocity(GameOptions.ballStartSpeed);

            // game state is now "Playing"
            this.gameState = GameState.Playing;
            return;
        }

        // do we have energy and are we still in game?
        if (this.energy > 0 && this.gameState != GameState.Over) {

            // set compound walls to active
            this.theCompound.setActive(true);

            // save the current timestamp
            this.downTime = e.downTime;
        }
    }

    // method to deactivate compound walls
    deactivateWalls(e : Phaser.Input.Pointer) : void {

        // if game state is set to "Waiting" the exit the function
        if (this.gameState == GameState.Waiting) { 
            return;
        }

        // are we playing?
        if (this.gameState == GameState.Playing) {

            // set compound walls to inactive
            this.theCompound.setActive(false);

            // determine how long the input has been pressed
            let ellapsedTime : number = Math.round(e.upTime - e.downTime);
            
            // subtract the ellapsed time from energy
            this.energy -= ellapsedTime;
        }
    }

    // method to get the edge fixture in a edge Vs circle contact
    getEdgeFixture(contact : planck.Contact) : planck.Fixture {

        // get first contact fixture
        let fixtureA : planck.Fixture = contact.getFixtureA(); 

        // get first contact shape
        let shapeA : planck.Shape = fixtureA.getShape();

        // is the shape an edge? Return the first contact fixture, else return second contact fixture
        return (shapeA.getType() == 'edge') ? fixtureA : contact.getFixtureB();
    }

    // method to be called at each frame
    update(time : number) : void {

        // is the compound object active?
        if (this.theCompound.active) {

            // determine remaining energy subtracting from current energy the amount of time we are pressing the input
            let remainingEnergy : number = this.energy - Math.round(time - this.downTime);

            // if remaining energy is less than zero...
            if (remainingEnergy <= 0) {

                // set energy to zero
                this.energy = 0;

                // turn off the compound
                this.theCompound.setActive(false);
            }
        }

        // advance the simulation by 1/30 seconds
        this.world.step(1 / 30);
  
        // crearForces  method should be added at the end on each step
        this.world.clearForces();

        // update ball position
        this.theBall.updatePosition();
        
        // draw walls of compound object
        this.theCompound.drawWalls(this.compoundGraphics);
    }
}