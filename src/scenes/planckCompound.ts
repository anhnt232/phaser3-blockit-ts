import * as planck from 'planck';
import { toMeters, toPixels } from './planckUtils';
 
export class PlanckCompound {

    // planck body
    planckBody : planck.Body;

    // flag to check if the compound is active
    active : boolean;

    // sprites to render the compound
    sprites : Phaser.GameObjects.Sprite[];
 
    constructor(scene : Phaser.Scene, world : planck.World, posX : number, posY : number, offset : number, tickness : number) {
 
        // this is how we create a generic Box2D body
        let compound : planck.Body = world.createKinematicBody();

        // set body position
        compound.setPosition(new planck.Vec2(toMeters(posX), toMeters(posY)))
        
        // add edge fixtures to body
        compound.createFixture(planck.Edge(new planck.Vec2(toMeters(0), toMeters(-offset)), new planck.Vec2(toMeters(offset), toMeters(0))));
        compound.createFixture(planck.Edge(new planck.Vec2(toMeters(offset), toMeters(0)), new planck.Vec2(toMeters(0), toMeters(offset))));
        compound.createFixture(planck.Edge(new planck.Vec2(toMeters(0), toMeters(offset)), new planck.Vec2(toMeters(-offset), toMeters(0))));
        compound.createFixture(planck.Edge(new planck.Vec2(toMeters(-offset), toMeters(0)), new planck.Vec2(toMeters(0), toMeters(-offset))));

        // give the compound object a random angle
        compound.setAngle(Phaser.Math.Angle.Random());

        // give the compound object an angular velocity, to make it rotate a bit
        compound.setAngularVelocity(0.1);

        // assign the body to planckBody property
        this.planckBody = compound;

        // calculate edge length with Pythagorean theorem
        let edgeLength : number = Math.sqrt(offset * offset + offset * offset);

        // create sprites to render the compound
        this.sprites = [
            scene.add.sprite(0, 0, 'wall'),
            scene.add.sprite(0, 0, 'wall'),
            scene.add.sprite(0, 0, 'wall'),
            scene.add.sprite(0, 0, 'wall')
        ]

        // loop through all sprites
        this.sprites.forEach((sprite : Phaser.GameObjects.Sprite) => {

            // adjust display size
            sprite.setDisplaySize(edgeLength + tickness * 2, tickness);

            // set sprite origin to horizontal center, vertical bottom
            sprite.setOrigin(0.5, 1);

            // set sprite to semi transparent
            sprite.setAlpha(0.3);
        });

        // the compound is not active at the beginning
        this.active = false;
    }

    // method to set the compound walls to active or non active
    setActive(isActive : boolean) : void {
        
        // loop through all sprites
        this.sprites.forEach((sprite : Phaser.GameObjects.Sprite) => {

            // set sprite to semi transparent if the compound is not active, fully opaque otherwise
            sprite.setAlpha(isActive ? 1 : 0.3);
        });
        
        // set acrive property according to method argument 
        this.active = isActive;
    }

    // method to draw compount walls in a Graphics game object
    drawWalls(graphics : Phaser.GameObjects.Graphics) : void {

        // clear the graphics
        graphics.clear();

        // set line style to one pixel black
        graphics.lineStyle(1, 0x000000);

        // just a counter to see how many fixtures we processed so far
        let fixtureIndex : number = 0;

        // loop through all body fixtures
        for (let fixture : planck.Fixture = this.planckBody.getFixtureList() as planck.Fixture; fixture; fixture = fixture.getNext() as planck.Fixture) {
            
            // get fixture edge
            let edge : planck.Edge = fixture.getShape() as planck.Edge;

            // get edge vertices
            let lineStart : planck.Vec2 = this.planckBody.getWorldPoint(edge.m_vertex1);
            let lineEnd : planck.Vec2 = this.planckBody.getWorldPoint(edge.m_vertex2);
            
            // turn the planck edge into a Phaser line
            let drawLine : Phaser.Geom.Line = new Phaser.Geom.Line(
                toPixels(lineStart.x),
                toPixels(lineStart.y),
                toPixels(lineEnd.x),
                toPixels(lineEnd.y)
            );

            // get line mid point
            let lineMidPoint : Phaser.Geom.Point = Phaser.Geom.Line.GetMidPoint(drawLine);
            
            // get line angle
            let lineAngle : number = Phaser.Geom.Line.Angle(drawLine);

            // stroke the line shape
            graphics.strokeLineShape(drawLine);

            // place the sprite in the middle of the edge
            this.sprites[fixtureIndex].setPosition(lineMidPoint.x, lineMidPoint.y);

            // rotate the sprite according to edge angle
            this.sprites[fixtureIndex].setRotation(lineAngle);

            // increase the counter processed texture
            fixtureIndex ++;
        }  
    }
}