// CONFIGURABLE GAME OPTIONS

export const GameOptions = {

    // unit used to convert pixels to meters and meters to pixels
    worldScale : 30,

    // ball radius, in pixels
    ballRadius : 20,

    // ball start speed, in meters/second
    ballStartSpeed : 3,

    // ball speed increase at each collision, in meters/second
    ballSpeedIncrease : 0.1,

    // wall width, in pixels
    wallTickness : 30,

    // wall edge distance from center, in pixels
    wallDistanceFromCenter : 350,

    // game energy, that is the amount in milliseconds of active wall time
    energy : 10000,

}